<?php
session_start();
if (isset($_POST['submit'])) {
	
	include_once 'dbh.inc.php';

	$first = mysqli_real_escape_string($conn, $_POST['first']);
	$last = mysqli_real_escape_string($conn, $_POST['last']);
	$email = mysqli_real_escape_string($conn, $_POST['email']);
	$phone = mysqli_real_escape_string($conn, $_POST['phone']);
	$date = mysqli_real_escape_string($conn, $_POST['date']);
	$uid = mysqli_real_escape_string($conn, $_POST['uid']);
	$pwd = mysqli_real_escape_string($conn, $_POST['pwd']);
	

	//error handelrs
	//check for empty fields

	if(empty($first) || empty($last) || empty($email) || empty($phone) || empty($date)  || empty($pwd)) {

			$_SESSION['message'] = "Fields are empty";
 			$_SESSION['msg_type'] = "danger";

		header("Location: ../signup.php?signup=empty");
		exit();

	} else {
		
		//check if input characters are valid
		if (!preg_match("/^[a-zA-Z]*$/", $first) || !preg_match("/^[a-zA-Z]*$/", $last)) {
			$_SESSION['message'] = "Invalid characters";
 			$_SESSION['msg_type'] = "danger";

			header("Location: ../signup.php?signup=invalid");
		exit();
		
	}else{
		//check if email is valid
	
	if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {

			$_SESSION['message'] = "Enter your email correctly";
 			$_SESSION['msg_type'] = "danger";
		header("Location: ../signup.php?signup=email");
		exit();
		} else{
			$sql= "SELECT * FROM p5_friends WHERE user_email = '$email' ";
			$result = mysqli_query($conn, $sql);
			$resultCheck = mysqli_num_rows($result);

			if ($resultCheck > 0) {
				$_SESSION['message'] = "User already taken";
 				$_SESSION['msg_type'] = "danger";
				header("Location: ../signup.php?signup=usertaken");
		exit();

			} else {
				// hashing the password
				$hashedPwd = password_hash($pwd, PASSWORD_DEFAULT);
				//INSERT THE USER INTO THE DATABASE
				$sql = "INSERT INTO p5_friends (user_first, user_last, user_email, user_phone, user_birth,  user_pwd) VALUES ('$first', '$last', '$email', '$phone', '$date', '$hashedPwd'); ";


				$_SESSION['message'] = "Successfully signed up!";
 				$_SESSION['msg_type'] = "success";
				 mysqli_query($conn, $sql);
				 header("Location: ../signup.php?signup=success");
			}
		}

	}
}


} else {
	header("Location: ../signup.php");
	exit();

} 