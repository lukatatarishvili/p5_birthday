<?php
include 'header.php';


if (isset($_SESSION['message'])): ?>

	 	<div class="alert alert-<?=$_SESSION['msg_type']?>">

	 	<?php 
	 	echo $_SESSION['message'];
	 	unset($_SESSION['message']); 
	 	?> 
	 	</div>	
	 	<?php endif; ?>


<section class="main-container">
	<div class="main-wrapper">
		<h2>Sign up</h2>
		<div class="form-group">
		<form class="signup-form" action="includes/signup.inc.php" method="POST">
			<div class="padd">
			<input type="text" class="form-control" name="first" placeholder="First name">
			</div>

			<div class="padd">
			<input type="text" class="form-control" name="last" placeholder="Last name">
			</div>

			<div class="padd">
			<input type="text" class="form-control" name="email" placeholder="E-mail">
			</div>
				<div class="padd">
			<input type="text" class="form-control" name="phone" placeholder="Phone number">
			</div>
			<div class="padd">
			<input type="date" class="form-control" name="date" >
			</div>
				<div class="padd">
			<input type="Password" class="form-control" name="pwd" placeholder="Password">
				</div>
				<div class="padd">
			<button type="submit" class="btn btn-success" name="submit">Sign up</button>
		</div>
		</div>

		</form>
		<div>
	</div>
</section>

<?php
include 'footer.php';
?>

